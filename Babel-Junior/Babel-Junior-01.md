---
title: Babel 入门教程（一）：什么是 Babel 及其配置文件
date: 2018-01-19 10:55:01
categories:
- Technique
tags:
  - JavaScript
---

## 前言
现在都用 ES6 新语法以及 ES7 新特性来写应用了，但是浏览器和相关的环境还不能友好的支持，需要用到 Babel 转码器来转换成 ES5 的代码。但是其插件之多，使用之稍微复杂，恰好团队现在也需要用 ES6 来开发 SaaS 后台，为了给大家普及一下，今天就来着重梳理一下有关 Babel 这个转码器的相关知识点，仅是一份 Babel 的入门教程系列，没有涉及到复杂的配置和插件的开发维护，大家斟酌着阅读即可。

相信大家都看到过如下的名词：
+ babel-preset-*
+ babel-plugin-*
+ babel-cli
+ babel-node
+ babel-core
+ babel-runtime
+ babel-plugin-transform-runtime
+ babel-polyfill
+ ...

到底都是些什么意思？有什么作用？相互之间有什么联系与区别？下面慢慢来一项项记录解答

## 什么是 Babel
Babel 是一个广泛使用的 ES6 转码器，可以将 ES6 代码转为 ES5 代码，从而在现有环境执行。这意味着，你可以用 ES6 的方式编写程序，又不用担心现有环境是否支持。下面是一个例子：
```js
// 转码前
input.map(item => item + 1);

// 转码后
input.map(function (item) {
  return item + 1;
});
```
上面的原始代码用了箭头函数，Babel 将其转为普通函数，就能在不支持箭头函数的 JavaScript 环境执行了。


## Babel 配置文件
使用 Babel 第一步就是在项目根目录创建其配置文件 `.babelrc`。
该文件是用来设置 Babel 转码器的转码规则和可调用插件，基本格式如下：
```json
{
  "presets": [],
  "plugins": []
}
```

注意：以下所有 Babel 工具和模块的使用，都必须先写好 `.babelrc`

### `presets` 字段
该字段设置转码规则

官方提供的功用转码规则有：
+ env
+ es2015
+ es2016
+ es2017
+ latest (不推荐, 请使用 env)
+ react
+ flow

常见的项目所使用的转码规则有如下：
```bash
# 根据环境自动决定合适插件的规则
$ npm i --save-dev babel-preset-env

# 最新转码规则
# 官方不推荐使用
$ npm i --save-dev babel-preset-latest

# 也相当于最新转码规则
$ npm i --save-dev babel-preset-es2015
$ npm i --save-dev babel-preset-es2016
$ npm i --save-dev babel-preset-es2017

# 针对不同阶段语法的转码规则（共有4个阶段，选装一个）
$ npm i --save-dev babel-preset-stage-0
$ npm i --save-dev babel-preset-stage-1
$ npm i --save-dev babel-preset-stage-2
$ npm i --save-dev babel-preset-stage-3

# react 语法转码规则
$ npm install --save-dev babel-preset-react
```

一般来说，使用 `babel-preset-env` 就足够了，如果是 react 项目，还可以搭配上 `babel-preset-react`:
```json
{
  "presets": [
    "env",
    "react"
  ],
  "plugins": []
}
```

### `plugins` 字段
可以使用的 plugins 就很多了，比如说我们经常使用的 `babel-plugin-transform-runtime` 或者是 `babel-plugin-transform-react-jsx`

安装相关 npm 包：
```bash
$ npm i -D babel-plugin-transform-runtime
$ npm i -D babel-plugin-transform-react-jsx
```

引入到 `.babelrc` 文件：
```json
{
  "presets": [],
  "plugins": [
    "transform-runtime",
    "transform-react-jsx"
  ]
}
```

**ES6 代码：**
```js
/** @jsx dom */
var { dom } = require("deku");

var profile = <div>
  <img src="avatar.png" className="profile" />
  <h3>{[user.firstName, user.lastName].join(' ')}</h3>
</div>;
```

**转译后代码：**
```js
/** @jsx dom */
var dom = require("deku").dom;

var profile = dom( "div", null,
  dom("img", { src: "avatar.png", className: "profile" }),
  dom("h3", null, [user.firstName, user.lastName].join(" "))
);
```
这里暂时只演示 `babel-plugin-transform-react-jsx` 插件的效果，关于 `babel-plugin-transform-runtime` 插件的在后面会详细讲

## 快速启动项目
如果你急需一份能够快速启动项目的、没有冗余配置的指南和配置，那么可以不问所以地进行如下操作。

安装相关依赖：
```bash
# 转译最新的基础语法
$ npm i -D babel-preset-env
# 转译 React jsx 的代码
$ npm i -D babel-preset-react
# 转译最新的 api 等，为低版本环境提供垫片，其会自动按需引入
$ npm i -D babel-plugin-transform-runtime
```

项目根目录中引入配置文件：
```json
{
  "presets": [
    "env",
    "react"
  ],
  "plugins": [
    "transform-runtime"
  ]
}
```

## 结语
本教程是连载的，本周内会结束该系列的所有分享。基本上都是写基本用法，只希望能够和大家一起理清楚脉络和联系。

如果你觉得本教程对你有用，欢迎转发分享。

对了，本叔今天第一次烘焙蛋挞，放少了糖，但是出来的味道还是很好，恰逢是低糖一族。hahahhhh...

---

<p style="text-align:center;">
    <img style="width:200px;" src="http://cdn.ironmaxi.com/images/wechat/qrcode.png" alt="微信公众号"/>
</p>
