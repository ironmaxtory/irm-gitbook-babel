---
title: Babel 入门教程（四）：babel-cli 与 babel-node 及工程实践
date: 2018-01-19 10:55:04
categories:
- Technique
tags:
  - JavaScript
---


## 前情提要
> Previously on Arrow...
> 'I wanna be sth else. I wanna be ... someone else.' -- Oliver Queen

<p style="color:#999;font-size:12px;">不抛《行尸走肉》的梗了，今天提《绿箭侠》... </p>


上一篇《Babel 入门教程（三）：babel-plugin-* 插件及与 babel-preset-* 预设插件的关系》，详细介绍了几个我们经常会用到的 `babel-plugin-*` 插件，而且这些插件和相近的 `babel-preset-*` 预设插件的区别联系以及使用场景，比较简单。

另外，可供使用的 `babel-preset-*` 与 `babel-plugin-*` 在 babel 官网和 npm 仓库都可以找到，链接如下：

babel 官网：[http://babeljs.io/docs/plugins/](http://babeljs.io/docs/plugins/)<br/>
npm 仓库：[https://www.npmjs.com/search?q=babel-plugin](https://www.npmjs.com/search?q=babel-plugin) 以及 [https://www.npmjs.com/search?q=babel-preset](https://www.npmjs.com/search?q=babel-preset)

## 前言
前面三节讲了很多关于如何安装和引入 `babel-preset-*` 和 `babel-plugin-*` 插件，包括它们的区别、联系和功能。但是一直没有讲到怎么利用这些插件去转译我们新语法的代码，相信大家已经手痒了，那今天就来一起转译吧！

老生常谈，再次提醒一遍，所有转译工作的基础，就是在我们的项目根目录要包含 babel 的配置文件 `.babelrc`!

## babel-cli
babel-cli 是 Babel 的命令行工具，我们可以利用其在终端中转译我们的代码

### 全局使用
安装 babel-cli
```bash
$ npm i -g babel-cli
```

基本用法如下：
```bash
# 转码结果输出到标准输出
$ babel example.js

# 转码结果写入一个文件
# --out-file 或 -o 参数指定输出文件
$ babel example.js --out-file compiled.js
# 等价于
$ babel example.js -o compiled.js

# 对整个目录转码
# --out-dir 或 -d 参数指定输出目录
$ babel src --out-dir lib
# 等价于
$ babel src -d lib

# -s 参数生成source map文件
$ babel src -d lib -s
```

但是为了能够多人协作开发项目，以及不同的项目可能使用到不同版本的 Babel，那么我们就有必要使用局部的 Babel 转换器了

### 局部使用
安装 babel-cli：
```bash
$ npm i -D babel-cli
```

然后补充项目中 package.json 的 `scripts` 字段内容：
```json
{
  "scripts": {
    "build": "babel src -d dist"
  }
}
```

转码的使用执行对应的脚本命令：
```bash
$ npm run build
```

Tips：合理地使用各种 cli 工具，配合 npm scripts，也就是 `package.json` 文件的 `scripts` 字段对应的命令可以打造很6的前端工作流。

## babel-node
babel-cli 工具自带一个 `babel-node` 命令，提供一个支持 ES6 的 REPL 环境。它支持 Node 的 REPL 环境的所有功能，而且可以直接运行 ES6 代码。

它不用单独安装，而是随 babel-cli 一起安装。然后，执行 `babel-node` 就进入 REPL 环境。

单独调试运行行内代码：
```bash
$ babel-node
> (x => x*2+1)(2)
5
```

或者运行 ES6 语法的文件：
```bash
$ babel-node ./es6/example.js
```

个人认为使用场景一般就是我们进行一些简单的逻辑或者语法验证。另外有没有发现它跟 `node` 的交互表现一模一样，对，就像那样使用就行了！

## 工程实践
### babel 结合 gulp 构建工具
安装相关依赖：
```bash
$ npm i -D gulp
$ npm i -D gulp-babel
# gulp-babel 依赖于 node_modules/babel-core
$ npm i -D babel-core
$ npm i -D babel-preset-env
```

新建 `.babelrc` 文件：
```json
{
  "presets": [
    "env"
  ],
  "plugins": []
}
```

新建 `gulpfile.js` 文件：
```js
var gulp = require("gulp");
var babel = require("gulp-babel");

gulp.task("default", function () {
  return gulp.src("src/example.js")
    .pipe(babel())
    .pipe(gulp.dest("lib"));
});
```

使用 `gulp` 构建工具来搭建前端自动化工作流不是本系列分享的重点，如果还有不了解 `gulp` 炫酷用法的同学，要抓紧去看看了。另外，别玩 `grunt` 了。


### babel 结合 webpack 模块化打包工具
安装相关依赖：
```bash
$ npm i -D webpack
$ npm i -D babel-loader
# babel-loader 依赖于 node_modules/babel-core
$ npm i -D babel-core
$ npm i -D babel-preset-env
```

新建 `.babelrc` 文件：
```json
{
  "presets": [
    "env"
  ],
  "plugins": []
}
```

新建 `webpack.config.js` 文件：
```js
module.exports = {
  entry: './src/app.js',
  output: {
    path: __dirname,
    filename: './bin/app.bundle.js',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: 'babel-loader'
    }]
  }
}
```

使用 `webpack` 模块化打包前端工程项目是近几年的大趋势，要是还不了解 `webpack` 的同学也是要快马加鞭了，现在出到 `webpack@3.x` 了，功能是越来越强大。

有人说它的入门门槛高，配置复杂，但我觉得世间上的事情都是相对的，虽然配置比较繁杂，但是它的功能真的是非常强大，能够把我们的项目模块化和打包得非常完美，使用过 `DLLPlugin` + `DLLReferencePlugin` 吗？了解过 `Tree-shaking` 代码优化技术吗？`webpack` 都做到了！

噢对，最近新出来一个号称 「零配置」的打包工具「Parcel」，github 上的下载量和 star 量远超 webpack，但是我试用了下，发现其生态还很不完善，很多插件都没有。好像我想通过路由配合 `*.vue` 文件来切换单页，并不能跑通，暂时弃坑了。

Parceljs 官网地址：[https://parceljs.org/](https://parceljs.org/)


## 结语
今天的内容较少较简单，强烈建议大家结合前几节内容，重新过一遍，然后写几份 ES6 代码，自行利用工具转译验证一下。

> 实践是验证真理的唯一标准。


---

<p style="text-align:center;">
    <img style="width:200px;" src="http://cdn.ironmaxi.com/images/wechat/qrcode.png" alt="微信公众号"/>
</p>
